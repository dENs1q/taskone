package com.company.task.two;

import java.util.Objects;

public class MyComplex {

    private double real = 0.0;
    private double imag = 0.0;

    public MyComplex() {
    }

    public MyComplex(double real, double imag) {
        this.real = real;
        this.imag = imag;
    }

    public double getReal() {
        return real;
    }

    public double getImag() {
        return imag;
    }

    public void setImag(double imag) {
        this.imag = imag;
    }

    public void setValue(double real, double imag) {
        this.real = real;
        this.imag = imag;
    }

    @Override
    public String toString() {
        if (imag == 0) {
            return "(" + real + ")";
        } else if (real == 0) {
            return "(" + imag + "i)";
        } else {
            if (imag > 0) {
                return "(" + real +
                        "+" + imag +
                        "i)";
            } else {
                return "(" + real +
                        "" + imag +
                        "i)";
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MyComplex myComplex = (MyComplex) o;
        return Double.compare(myComplex.real, real) == 0 &&
                Double.compare(myComplex.imag, imag) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(real, imag);
    }

    public boolean isReal() {
        return imag == 0;
    }

    public void setReal(double real) {
        this.real = real;
    }

    public boolean isImaginary() {
        return real == 0;
    }

    public boolean equals(double real, double imag) {
        return this.real == real
                && this.imag == imag;
    }

    public boolean equals(MyComplex another) {
        if (another == null) {
            return false;
        }
        if (this == another) {
            return true;
        }
        return this.real == another.getReal()
                && this.imag == another.getImag();
    }

    public double magnitude() {
        return Math.sqrt(real * real + imag * imag);
    }

    //           arctg(im/re)     , x > 0;
    // arctg z = arctg(im/re) + pi, x < 0, y >= 0;
    //           arctg(im/re) - pi, x < 0, y < 0;
    public double argument() {
        if (real < 0
                && imag >= 0) {
            return Math.atan(imag / real + Math.PI);
        } else if (real < 0
                && imag < 0) {
            return Math.atan(imag / real - Math.PI);
        }
        return Math.atan(imag / real);
    }

    public MyComplex add(MyComplex right) {
        this.imag += right.getImag();
        this.real += right.getReal();
        return this;
    }

    public MyComplex addNew(MyComplex right) {
        MyComplex newMyComplex = new MyComplex(this.imag, this.real);
        return newMyComplex.add(right);
    }

    public MyComplex subtract(MyComplex right) {
        this.imag -= right.getImag();
        this.real -= right.getReal();
        return this;
    }

    public MyComplex subtractNew(MyComplex right) {
        MyComplex newMyComplex = new MyComplex(this.imag, this.real);
        return newMyComplex.subtract(right);
    }

    public MyComplex multiply(MyComplex right) {
        double real = this.real * right.getReal() - this.imag * right.getImag();
        double imag = this.imag * right.getReal() + this.real * right.getImag();
        this.real = real;
        this.imag = imag;
        return this;
    }

    // a1*a2+b1*b2    a2*b1−a1*b2
    // ----------- + ----------- i
    // a2^2+b2^2      a2^2+b2^2
    public MyComplex divide(MyComplex right) {
        double real = (this.real * right.real + this.imag * right.imag)
                /
                (right.real * right.real + right.imag * right.imag);
        double imag = (right.real * this.imag - this.real * right.imag)
                /
                (right.real * right.real + right.imag * right.imag);
        this.real = real;
        this.imag = imag;
        return this;
    }

    public MyComplex conjugate() {
        this.imag = -this.imag;
        return this;
    }

}
