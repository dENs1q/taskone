package com.company.task.two;

import java.util.Arrays;

public class MyPolynomial {

    private double[] coeffs;

    public MyPolynomial(double[] coeffs) {
        this.coeffs = coeffs;
    }

    public int getDegree() {
        return coeffs.length - 1;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (int i = coeffs.length - 1; i >= 0; i--) {
            if (i != coeffs.length - 1) {
                if (coeffs[i] >= 0) {
                    result.append("+");
                }
            }
            if (coeffs[i] != 0) {
                if (i == 1) {
                    result.append(coeffs[i]).append("x");
                } else if (i == 0) {
                    result.append(coeffs[i]);
                } else {
                    result.append(coeffs[i]).append("x^").append(i);
                }
            }
        }
        return result.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MyPolynomial that = (MyPolynomial) o;
        return Arrays.equals(coeffs, that.coeffs);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(coeffs);
    }

    public double evaluate(double x) {
        double result = 0;
        for (int i = coeffs.length - 1; i >= 0; i--) {
            result += coeffs[i] * Math.pow(x, i);
        }
        return result;
    }

    //Create a new array the big of two polynomials. Add numbers with equal indices. Create a new polynomial from a new array.
    public MyPolynomial add(MyPolynomial right) {
        int max = Math.max(this.getDegree(), right.getDegree()) + 1;
        double[] newPolinom = new double[max];
        for (int i = 0; i <= max; i++) {
            if (this.coeffs.length > i) {
                newPolinom[i] = coeffs[i];
            }
            if (right.coeffs.length > i) {
                newPolinom[i] += right.coeffs[i];
            }
        }
        return new MyPolynomial(newPolinom);
    }
    // multiply each value
    public MyPolynomial multiply(MyPolynomial right) {
        double[] newPolinom = new double[this.getDegree() + right.getDegree() + 1];
        for (int i = this.getDegree(); i >= 0; i--) {
            for (int j = right.getDegree(); j >= 0; j--) {
                newPolinom[i + j] += this.coeffs[i] * right.coeffs[j];
            }
        }
        return new MyPolynomial(newPolinom);
    }
}
