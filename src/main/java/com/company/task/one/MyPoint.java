package com.company.task.one;

import java.util.Objects;

public class MyPoint {
    private int x = 0;
    private int y = 0;

    public MyPoint() {
    }

    public MyPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int[] getXY() {
        int[] pointXY = new int[2];
        pointXY[0] = x;
        pointXY[1] = y;
        return pointXY;
    }

    public void setXY(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "(" + x +
                "," + y +
                ")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MyPoint myPoint = (MyPoint) o;
        return x == myPoint.x &&
                y == myPoint.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    // AB = √(xb - xa)2 + (yb - ya)2
    public double distance(int x, int y) {
        int xDiff = x - this.x;
        int yDiff = y - this.y;

        return Math.sqrt(xDiff * xDiff + yDiff * yDiff);
    }

    public double distance(MyPoint another) {
        int xDiff = another.getX() - this.x;
        int yDiff = another.getY() - this.y;

        return Math.sqrt(xDiff * xDiff + yDiff * yDiff);
    }

    public double distance() {
        return distance(0,0);
    }
}
