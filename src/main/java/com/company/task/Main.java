package com.company.task;


import com.company.task.one.MyPoint;
import com.company.task.two.Ball;
import com.company.task.two.Container;
import com.company.task.two.MyComplex;
import com.company.task.two.MyPolynomial;

public class Main {
    public static void main(String[] args) {

        MyPoint myPoint1 = new MyPoint(1, 1);
        MyPoint myPoint2 = new MyPoint(2, 2);

        System.out.println("myPoint = " + myPoint1);
        System.out.println("myPoint.getXY() = " + myPoint1.distance(myPoint2));
        System.out.println("---------------------------------------------------------");

        MyComplex myComplex1 = new MyComplex(2, 5);
        MyComplex myComplex2 = new MyComplex(3, 6);

        System.out.println("myComplex1.add(myComplex2) = " + myComplex1.add(myComplex2));
        System.out.println("myComplex1.subtract(myComplex2) = " + myComplex1.subtract(myComplex2));
        System.out.println("myComplex1.argument() = " + myComplex1.argument());
        System.out.println("myComplex1.subtractNew(myComplex2) = " + myComplex1.subtractNew(myComplex2));
        //System.out.println("myComplex1.multiply(myComplex2) = " + myComplex1.multiply(myComplex2));
        System.out.println("myComplex1.divide(myComplex2) = " + myComplex1.divide(myComplex2));
        System.out.println("---------------------------------------------------------");

        double[] arr1 = new double[]{2, 5, 1, 3, 6};
        double[] arr2 = new double[]{3, 6, 2, 4, 7, 4};
        MyPolynomial myPolynomial1 = new MyPolynomial(arr1);
        MyPolynomial myPolynomial2 = new MyPolynomial(arr2);
        System.out.println("myPolynomial1 = " + myPolynomial1);
        System.out.println("myPolynomial1.evaluate = " + myPolynomial1.evaluate(2));
        System.out.println("myPolynomial1.add(arr2) = " + myPolynomial1.add(myPolynomial2));
        System.out.println("myPolynomial1.multiply(myPolynomial2) = " + myPolynomial1.multiply(myPolynomial2));
        System.out.println("---------------------------------------------------------");

        Ball ball = new Ball(0,9,1,2,150);
        Container container = new Container(0,0,20,20);
        System.out.println("container.collides(ball) = " + container.collides(ball));
        ball.move();
        System.out.println("ball = " + ball);
        System.out.println("container.collides(ball) = " + container.collides(ball));
    }
}
